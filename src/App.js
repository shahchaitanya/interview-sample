import React, { Component } from "react";
import _ from "lodash";
import "./App.css";

//There are 1000 number of users
const elemnets = 1000;

//There are following columns. Each column represents a property of a user.
const FIELDCOLUMN = [
  { label: "Name", fieldKey: "name" },
  { label: `Father's Name`, fieldKey: "fName" },
  { label: `Mother's Name`, fieldKey: "mName" },
  { label: `Email`, fieldKey: "email" },
  { label: `Contact No`, fieldKey: "contactNum" },
  { label: "City", fieldKey: "city" },
  { label: "Country", fieldKey: "country" },

  { label: "Delete", fieldKey: "delete" }
];
class App extends Component {
  constructor(props) {
    super(props);

    //Here userList is array of object. Here it is initialized with 1000 items.
    this.state = {
      userList: Array.apply(null, Array(elemnets)).map(() => {
        return {};
      })
    };
  }

  //This function is triggered when you update the property of any user
  onInputFieldChange = ({ index, value, fieldKey }) => {
    const userList = this.state.userList;
    userList[index][fieldKey] = value;
    this.setState({});
  };

  //This function is triggered when you add a new user
  addUser = () => {
    const userList = this.state.userList;
    userList.splice(0, 0, {});
    this.setState({ userList });
  };

  //This function is triggered when you delete a user
  onClickDelete = ({ index }) => {
    const userList = this.state.userList;
    userList.splice(index, 1);
    this.setState({ userList });
  };

  /*
  Typically a userList will look like this
  [
    {"firstName":"Chaitu","lastName":"Shah"},
    {"firstName":"Ashish"},
    {"firstName":"Nikhil","email":"nikhil@toddleapp.com"},
  ]
  */

  render() {
    const { userList } = this.state;

    return (
      <div className="container">
        <div className="header">
          <div className="headerText">{`Users`}</div>
          <div className="button" onClick={this.addUser}>{`Add User`}</div>
        </div>
        <div className="scrollContainer">
          <div className="listContainer">
            <div className={"row headerRow"}>
              {_.map(FIELDCOLUMN, ({ label }) => {
                //Render Header Row
                return <div className={"headerColumn"}>{label}</div>;
              })}
            </div>
            {_.map(userList, (user, rowIndex) => {
              //Render each user
              return (
                <div className="row">
                  {_.map(FIELDCOLUMN, ({ label, fieldKey }, colIndex) => {
                    //Render each column
                    return (
                      <div className={"headerColumn"}>
                        {fieldKey != "delete" ? (
                          <input
                            type="text"
                            className={"inputField"}
                            value={user[fieldKey] || ""}
                            onChange={e =>
                              this.onInputFieldChange({
                                value: e.target.value,
                                index: rowIndex,
                                fieldKey
                              })
                            }
                          ></input>
                        ) : (
                          <div
                            className={"delete"}
                            onClick={() =>
                              this.onClickDelete({ index: rowIndex })
                            }
                          >
                            {"Delete"}
                          </div>
                        )}
                      </div>
                    );
                  })}
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}

export default App;
